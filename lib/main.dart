import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:async';

enum PageTransitionType {
  fade,
  rightToLeft,
  leftToRight,
  upToDown,
  downToUp,
  scale,
  rotate,
  size,
  rightToLeftWithFade,
  leftToRightWithFade,
}

class PageTransition<T> extends PageRouteBuilder<T> {
  final Widget child;
  final PageTransitionType type;
  final Curve curve;
  final Alignment alignment;
  final Duration duration;

  PageTransition({
    Key key,
    @required this.child,
    @required this.type,
    this.curve = Curves.linear,
    this.alignment,
    this.duration = const Duration(milliseconds: 300),
  }) : super(
            pageBuilder: (BuildContext context, Animation<double> animation,
                Animation<double> secondaryAnimation) {
              return child;
            },
            transitionDuration: duration,
            transitionsBuilder: (BuildContext context,
                Animation<double> animation,
                Animation<double> secondaryAnimation,
                Widget child) {
              switch (type) {
                case PageTransitionType.fade:
                  return FadeTransition(opacity: animation, child: child);
                  break;
                case PageTransitionType.rightToLeft:
                  return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
                  break;
                case PageTransitionType.leftToRight:
                  return SlideTransition(
                    transformHitTests: false,
                    position: Tween<Offset>(
                      begin: const Offset(-1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
                  break;
                case PageTransitionType.upToDown:
                  return SlideTransition(
                    transformHitTests: false,
                    position: Tween<Offset>(
                      begin: const Offset(0.0, -1.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(0.0, 1.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
                  break;
                case PageTransitionType.downToUp:
                  return SlideTransition(
                    transformHitTests: false,
                    position: Tween<Offset>(
                      begin: const Offset(0.0, 1.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(0.0, -1.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
                  break;
                case PageTransitionType.scale:
                  return ScaleTransition(
                    alignment: alignment,
                    scale: CurvedAnimation(
                      parent: animation,
                      curve: Interval(
                        0.00,
                        0.50,
                        curve: curve,
                      ),
                    ),
                    child: child,
                  );
                  break;
                case PageTransitionType.rotate:
                  return new RotationTransition(
                    alignment: alignment,
                    turns: animation,
                    child: new ScaleTransition(
                      alignment: alignment,
                      scale: animation,
                      child: FadeTransition(
                        opacity: animation,
                        child: child,
                      ),
                    ),
                  );
                  break;
                case PageTransitionType.size:
                  return Align(
                    alignment: alignment,
                    child: SizeTransition(
                      sizeFactor: CurvedAnimation(
                        parent: animation,
                        curve: curve,
                      ),
                      child: child,
                    ),
                  );
                  break;
                case PageTransitionType.rightToLeftWithFade:
                  return SlideTransition(
                    position: Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: FadeTransition(
                      opacity: animation,
                      child: SlideTransition(
                        position: Tween<Offset>(
                          begin: Offset.zero,
                          end: const Offset(-1.0, 0.0),
                        ).animate(secondaryAnimation),
                        child: child,
                      ),
                    ),
                  );
                  break;
                case PageTransitionType.leftToRightWithFade:
                  return SlideTransition(
                    position: Tween<Offset>(
                      begin: const Offset(-1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: FadeTransition(
                      opacity: animation,
                      child: SlideTransition(
                        position: Tween<Offset>(
                          begin: Offset.zero,
                          end: const Offset(1.0, 0.0),
                        ).animate(secondaryAnimation),
                        child: child,
                      ),
                    ),
                  );
                  break;
                default:
                  return FadeTransition(opacity: animation, child: child);
              }
            });
}

class Photos {
  static List<String> urls = [
    'https://images.unsplash.com/photo-1579753626276-0700e71e64bd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1567146295017-af5941244fbf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1581355707867-e6bbfdceace3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1553856622-d1b352e9a211?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1551813520-7e068e6fffa3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1551639724-a6756756dbde?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1559406087-def67db248c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1556898578-bbb39c1cb265?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1569687588613-e414cd1568e4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1570327920374-cad141a2d341?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1592029383200-73fb26a5b925?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1581552801758-ccba6e259090?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1584400412929-58a7735a0efb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1588650944142-c9160fd45802?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1587069304007-7387e396cd38?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1576967852544-8c3f18ac86a1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1570025796121-87d2d9b69ca9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1561239216-8d8b6afca9d6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1564384090881-f0d51c896fce?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
    'https://images.unsplash.com/photo-1546072921-51e1a830f091?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60',
  ];

  static Random _random = Random();

  static String generateFileSize() {
    return '${_random.nextInt(4) + 1},${_random.nextInt(10)} MB';
  }
}

void main() => runApp(GalleryApp());

class Bloc {
  // ignore: close_sinks
  final _themeController = StreamController<bool>();
  get changeTheme => _themeController.sink.add;
  get darkThemeEnabled => _themeController.stream;
}

final bloc = Bloc();

class GalleryApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: bloc.darkThemeEnabled,
      initialData: false,
      builder: (context, snapshot) => MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: snapshot.data ? ThemeData.dark() : ThemeData.light(),
          home: PhotoGrid()),
    );
  }
}

class ThemeIcon {
  static Icon defaultIcon() {
    return Icon(Icons.wb_sunny_outlined);
  }

  static Icon getIcon(isChecked) {
    if (isChecked) {
      return Icon(Icons.wb_sunny);
    } else {
      return Icon(Icons.wb_sunny_outlined);
    }
  }
}

void addImage(state) {
  showDialog(
    context: state.context,
    builder: (context) => AlertDialog(
      content: TextField(
        autofocus: true,
        onSubmitted: (value) {
          if (!(value == null || value == "")) {
            Photos.urls.add(value);
          }
          state.updateUrls();
          Navigator.pop(context);
        },
      ),
      title: Text('Bild hinzuzufügen'),
    ),
  );
}

void promptRemoveImageItem(index, state) {
  showDialog(
    context: state.context,
    builder: (context) => AlertDialog(
      content: Image.network(state._urls[index]),
      title: Text('Bild Löschen?'),
      actions: <Widget>[
        FlatButton(
          child: Text('ABBRECHEN'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        FlatButton(
          child: Text('LÖSCHEN'),
          onPressed: () {
            Photos.urls.removeAt(index);
            state.updateUrls();
            Navigator.pop(context);
          },
          textColor: Colors.red,
        ),
      ],
    ),
  );
}

class PhotoGrid extends StatefulWidget {
  @override
  _PhotoGridState createState() => _PhotoGridState();
}

class _PhotoGridState extends State<PhotoGrid> {
  var _urls = Photos.urls;
  Icon _icon = ThemeIcon.defaultIcon();
  bool isChecked = false;

  void updateUrls() {
    setState(() {
      _urls = Photos.urls;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("GalleryApp - Grid"),
        actions: <Widget>[
          IconButton(
            icon: _icon,
            onPressed: () => setState(() {
              setState(() {
                isChecked = !isChecked;
                _icon = ThemeIcon.getIcon(isChecked);
                bloc.changeTheme(isChecked);
              });
            }),
          ),
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () => addImage(this),
          ),
          IconButton(
            icon: const Icon(Icons.grid_on),
            onPressed: () => Navigator.push(
              context,
              PageTransition(
                type: PageTransitionType.fade,
                child: PhotoList(this),
              ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: GridView.count(
          crossAxisCount: 3,
          mainAxisSpacing: 16.0,
          crossAxisSpacing: 16.0,
          children: List.generate(
            _urls.length,
            (index) {
              return GestureDetector(
                onTap: () => promptRemoveImageItem(index, this),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(_urls[index]),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class PhotoList extends StatefulWidget {
  final _PhotoGridState gridState;
  PhotoList(this.gridState);

  @override
  _PhotoListState createState() => _PhotoListState(gridState);
}

class _PhotoListState extends State<PhotoList> {
  final _PhotoGridState gridState;
  _PhotoListState(this.gridState);

  Icon _icon;
  bool isChecked;

  var _urls = Photos.urls;

  void updateUrls() {
    setState(() {
      _urls = Photos.urls;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_icon == null) {
      _icon = ThemeIcon.getIcon(gridState.isChecked);
      isChecked = gridState.isChecked;
    }

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("GalleryApp - List"),
        actions: <Widget>[
          IconButton(
            icon: _icon,
            onPressed: () => setState(() {
              setState(() {
                isChecked = !isChecked;
                _icon = ThemeIcon.getIcon(isChecked);
                bloc.changeTheme(isChecked);
              });
            }),
          ),
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () => addImage(this),
          ),
          IconButton(
            icon: const Icon(Icons.grid_off),
            onPressed: () => {
              gridState.updateUrls(),
              gridState.isChecked = isChecked,
              gridState._icon = _icon,
              Navigator.pop(context)
            },
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: ListView.separated(
          itemCount: _urls.length,
          separatorBuilder: (BuildContext context, int index) => Divider(),
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              onTap: () => promptRemoveImageItem(index, this),
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.network(
                  _urls[index],
                  width: 50,
                  height: 50,
                ),
              ),
              title: Text(
                Photos.generateFileSize(),
              ),
            );
          },
        ),
      ),
    );
  }
}
